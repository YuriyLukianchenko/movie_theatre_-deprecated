package ua.epam.spring.hometask.repo;

import ua.epam.spring.hometask.domain.User;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class UserRepository {

    private static Map<Long, User> userMap = new TreeMap<>();

    public void save(User user) {
        userMap.put(user.getId(), user);
    }

    public void remove(User user) {
        userMap.remove(user.getId());
    }

    public User getById(Long userId) {
        return userMap.get(userId);
    }

    public Set<User> getAll() {
        return new HashSet<>(userMap.values());
    }
}
