package ua.epam.spring.hometask.service;

import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.repo.UserRepository;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public class UserServiceImpl implements UserService {

    private UserRepository userRepository;


    @Override
    public User save(@Nonnull User  user) {
        String id = UUID.randomUUID().toString();
        user.setId(Long.parseLong(id));
        userRepository.save(user);
        return user;
    }

    @Override
    public void remove(@Nonnull User user) {
        Optional.of(user.getId())
                .ifPresent(id -> Optional.of(userRepository.getById(user.getId()))
                        .ifPresent(localUser -> userRepository.remove(user)));
    }

    @Override
    public User getById(@Nonnull Long userId) {
        return userRepository.getById(userId);
    }

    @Override
    public @Nonnull Set<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getUserByEmail( @Nonnull String email) {
        Optional<User> optional = userRepository.getAll().stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst();

        return optional.isPresent() ? optional.get() : null;
    }
}
